/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

struct ActorInfo {
    uint hashId <format=hex, bgcolor=0x00FFEE>;
    int STRHash <format=hex>;
    int shapeInfoStart <bgcolor=0xBBFFFF>;
    int shapeInfoEnd;
};

struct ActorNumber {
    uint num[2] <bgcolor=0xFF98588>;
    uint zero  <bgcolor=0x7798588>;
};

struct HkscFile
{
    FileHeader fileHeader;
    local int64 dataOffset = FTell();
    
    int offsetEOF <bgcolor=0x00BB00>;
    Counter actorCount <bgcolor=0x2398588>;
    Counter actorCount2 <bgcolor=0x2395088>;
    FSkip(4); // padding
    
    ActorInfo actors[actorCount.count] <bgcolor=0x66FFEE>;
    ActorNumber actorNum[actorCount2.count];
    alignPointer(16);

    FSeek(fileHeader.sectionData.ptr[2] + dataOffset); 
    while(FTell() < fileHeader.sectionData.ptr[7] + dataOffset)
    {
        uint pointerSection <bgcolor=0x8800AA>;
    }
};