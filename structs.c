/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

struct DynamicString {
    string name;
    FSkip(-1);
    alignPointer(16);
};

// counter used in many classes, count and count2 are the same
struct Counter {
    int padding;
    int count;
    ushort flag;
    ushort count2; 
};

struct Vector4 {
    float x;
    float y;
    float z;
    float w;
};

struct TransposedMatrix3x4
{   
    float x[4];
    float y[4];
    float z[4];
};

// plane equation in the form of: ax + by + cz + d = 0
struct Plane {
    float a;
    float b;
    float c;
    float d;
};

// material name, is aligned to 2 bytes
struct MaterialName
{
    string name;
    if(ReadByte() == 0)
    {
        FSkip(1);
    }
};

// used at the end of the file, maps two objects together
struct TypePointer {
    uint ptrStart <bgcolor=0x444444>;
    int flag;
    uint ptrEnd <bgcolor=0x999999>;
};

// used at the end of the file, maps data to a class at the top
struct ClassPointer {
    uint ptrData <bgcolor=0x444444>; // relative to the start of the data
    int flag;
    uint ptrClass <bgcolor=0x999999>; // relative to the class name section
};