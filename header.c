/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

struct FileHeaderSection { 
    string name;
    alignPointer(16);

    uint ptr[8] <format=hex, bgcolor=0x883333>;
};
struct FileHeaderClass {
    uint id <format=hex>;
    char tabChar;
    string name;
};

struct FileHeader {
    char magic[8];
    uint flags;
    uint version;
    ubyte unknown0;
    ubyte bom;
    ubyte unknown1;
    ubyte unknown2;
    uint PackFileSectionCount;
    uint maybeSectionIndex;
    uint padding00[2] <hidden=true>;
    char padding01[3] <hidden=true>;
    char havok_version[17];

    int padding02 <hidden=true>; 
    int extraDataMarker;
    if(extraDataMarker & 0x10) { // NEW
        FSkip(16);
    }


    FileHeaderSection sectionClasses;
    FSkip(16); // padding
    FileHeaderSection sectionTypes;
    FSkip(16); // padding
    FileHeaderSection sectionData;
    FSkip(16); // padding
    
    local uint classOffset = FTell();
    local uint classNameCount = countClassNames();
    FileHeaderClass classes[classNameCount] <bgcolor=0x663399, optimize=false>;
    
    //uint classEndMarker <hidden=true>; 
    alignPointer(16);
};