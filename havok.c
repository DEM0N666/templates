/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

BigEndian();

#include "./structs.c"
#include "./functions.c"
#include "./header.c"
#include "./hskc.c"
#include "./classes.c"

struct 
{   
    local string fileType = SubStr(FileNameGetExtension(GetFileNameW()), 1);
    Printf("File-Type: %s\n", fileType);

    if(fileType == "hksc")
    {
        HkscFile hksc;
    }

    FileHeader fileHeader;
    local int64 dataOffset = FTell();
    local uint ptrChunks  = fileHeader.sectionData.ptr[2] + dataOffset;
    local uint ptrEntries = fileHeader.sectionData.ptr[3] + dataOffset;
    local uint ptrClasses = fileHeader.sectionData.ptr[4] + dataOffset;
    
    FSeek(ptrChunks);
    while(FTell() < ptrEntries && ReadInt() != 0xFFFFFFFF) {
        uint chunkPointer <bgcolor=0x8800AA>;

        Printf("Chunk-Ptr @ 0x%X\n", dataOffset + chunkPointer);
    }
    Printf("\n");    

    FSeek(ptrEntries);
    while(FTell() < ptrClasses && ReadInt() != 0xFFFFFFFF) {
        TypePointer entryPointer <bgcolor=0x8800FF>;
        Printf("Entry-Link @ 0x%X => 0x%X\n", 
            dataOffset + entryPointer.ptrStart, 
            dataOffset + entryPointer.ptrEnd
        );
    }
    Printf("\n");

    // Read class data
    local string className;
    local uint classOffset;
    local int oldOffset;

    FSeek(ptrClasses);

    while(!FEof() && ReadInt() != 0xFFFFFFFF) {
        ClassPointer classPointer <bgcolor=0x8866FF>;

        className = ReadLine(fileHeader.classOffset + classPointer.ptrClass);
        classOffset = dataOffset + classPointer.ptrData;
        Printf("Chunk @ 0x%X => '%s'", classOffset, className);

        oldOffset = FTell();
        FSeek(classOffset);
    
        switch(className)
        {
            case "hkRootLevelContainer":    RootLevelContainer rootLevelContainer;  break;
            case "hkpPhysicsData":          PhysicsData physicsData;                break;
            case "hkpPhysicsSystem":        PhysicsSystem physicSystem;             break;
            case "hkpRigidBody":            RigidBody rigidBody;                    break;
            case "hkpStaticCompoundShape":  StaticCompoundShape compoundShape;      break;
            case "hkpListShape":            ListShape listShape;                    break;

            case "hkpBoxShape":            BoxShape boxShape;                       break;
            case "hkpSphereShape":         SphereShape sphereShape;                 break;
            case "hkpConvexVerticesShape": ConvexVerticesShape convexVerticesShape; break;

            case "hkpConvexTranslateShape":  ConvexTranslateShape convexTranslateShape; break;
            case "hkpConvexTransformShape":  ConvexTransformShape convexTransformShape; break;
            case "hkpBvCompressedMeshShape": CompressedMesh compressedMesh;             break;

            default:
                Printf("(Unknown class)");
            break;
        }

        Printf("\n");
        FSeek(oldOffset);
    }

} HkFile;
