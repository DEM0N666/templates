/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

struct PhysicsSystem
{
    FSkip(4 * 2);
    Counter entries <bgcolor=0xFF0000>;
    Counter unknownCounters[3] <bgcolor=0xFF7777>;
    alignPointer(16);

    uint unknownInt <bgcolor=0xFF9999>;
    alignPointer(16);

    FSkip(16 * entries.count);

    string name <bgcolor=0xFF7777>;
};