
struct ListShape {
    int flags[4] <bgcolor=0x00AAAA>;
    FSkip(4 * 2);

    Counter entryCount <bgcolor=0x66AAAA>;
    alignPointer(16);

    Vector4 globalHalfExtends <bgcolor=0x885555>;
    Vector4 globalCenter <bgcolor=0x885555>;

    FSkip(16 * 2); // padding
    
    Vector4 unknownBoxesVec[entryCount.count] <bgcolor=0x774444>;

    //CollBoxShape boxes[entryCount.count];
};