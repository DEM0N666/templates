struct CompressedMeshIndex {
    ubyte a <bgcolor=0x55AA55>;
    ubyte b <bgcolor=0x55CC55>;
    ubyte c <bgcolor=0x55FF55>;
    ubyte d <bgcolor=0x55FF55>;
};

struct CompressedMeshDataRun
{
    uint data <bgcolor=0xBB9944>;
    ubyte index <bgcolor=0xFF9944>;
    ubyte count;
    FSkip(2);
};

struct CompressedMesh
{
    int unknown0[8] <bgcolor=0x559955>;
    Counter counter0 <bgcolor=0x99FF99>;
    Counter counter1 <bgcolor=0x66FF66>;
    Counter materiaCount <bgcolor=0x99FF99>;
    alignPointer(16);
    Counter counter3 <bgcolor=0x66FF66>;
    alignPointer(16);

    Vector4 aabbMin <bgcolor=0xFF9944>;
    Vector4 aabbMax <bgcolor=0xFF9966>;
        
    int primitiveKeyCount;
    int bitsPerKey <bgcolor=0xBB9944>;
    int maxKeyVal;

    Counter counter4 <bgcolor=0x99FF99>;
    Counter counterIndices <bgcolor=0x66FF66>;
    Counter counter6 <bgcolor=0x99FF99>;
    Counter countPackedVertices <bgcolor=0x66FF66>;
    Counter counter8 <bgcolor=0x99FF99>;
    Counter counterDataRuns <bgcolor=0x66FF66>;

    alignPointer(16);
    
    int unknown2[2];
    FSkip(4 * 2);

    int unknown3[counter1.count] <bgcolor=0x669966>;
    alignPointer(16);
    FSkip(4 * materiaCount.count);

    MaterialName material[materiaCount.count] <bgcolor=0xFF9944, optimize=false>;
    alignPointer(16);
    
    byte unknown4[counter3.count * 5];
    alignPointer(16);

    struct UnknownData4 {
        Counter counter10 <bgcolor=0x66FF66>;
        FSkip(4);

        float data0[14] <bgcolor=0x22FF66>;
        FSkip(4);

        int sharedVertices <bgcolor=0x22AA33>;
        int primitives <bgcolor=0x22AA32>;
        int dataRunsCount <bgcolor=0x22AA33>;
        ubyte packedVerticesCount <bgcolor=0x22AA32>;

        alignPointer(16);        
    } unknownData4[counter4.count] <optimize=false>;
    
    local int i=0;
    for(i=0; i<counter4.count; ++i)
    {
        struct UnknownData10 {
            int data10[unknownData4[i].counter10.count] <bgcolor=0xBB9944, optimize=false>;
        } data10 <optimize=false>;
        alignPointer(16);
    }

    // indices in pairs of four, the last two are (in most cases) the same
    CompressedMeshIndex indices[counterIndices.count];
    alignPointer(16);

    ushort indices2[counter6.count] <bgcolor=0x22AA33, optimize=false>;
    alignPointer(16);

    // vertext data, each integer has 3 integers for XYZ packed in with 10, 11 and 11 bits each
    // the integers are then used to map to the aabbMin, aabbMax array at the top
    int packedVertices[countPackedVertices.count] <bgcolor=0x22FF66>;
    alignPointer(16); 

    uint indices3[counter8.count * 2] <bgcolor=0x55AA22, optimize=false>;
    alignPointer(16);

    CompressedMeshDataRun dataRuns[counterDataRuns.count] <bgcolor=0xBB9944, optimize=false>;
    alignPointer(16); 
};