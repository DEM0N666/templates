/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

struct BoxShape 
{
    int padding00[2];
    
    byte type;
    byte shapeDispatchType;
    byte bitsPerKey;
    byte codecInfo;

    int someInt;

    float radius <name="Radius??", bgcolor=0x883333>;
    FSkip(3 * 4);
    Vector4 vec <bgcolor=0x885555>;
};
